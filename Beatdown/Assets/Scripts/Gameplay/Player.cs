﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	public CharacterController controller;
    public Conductor conductor;
	public float walkSpeed;
	public float sprintSpeed;
	public float gravity;
	public float groundDistance;
	public float jumpHeight;
	public Transform groundCheck;
	public LayerMask groundMask;
    public AudioSource[] walks;
    public AudioSource[] runs;
    public float stepsPerSec;

	float speed;
    float timeSinceLastStep;
	Vector3 velocity;
	bool isGrounded;
    bool wasGrounded;
    bool soundReady;

    void Start()
    {
        timeSinceLastStep = 1f;
        StartCoroutine(Wait(0.125f));
    }
    IEnumerator Wait(float s)
    {
        yield return new WaitForSeconds(s);
        soundReady = true;
    }

    void Update()
    {
    	isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
    	if(isGrounded && velocity.y < 0) velocity.y = gravity/5;

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        if(Input.GetKey(KeyCode.LeftShift)) speed = sprintSpeed;
        else speed = walkSpeed;

        Vector3 move = transform.right * x + transform.forward * z;
        controller.Move(move * speed);

        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
            MoveSound("run");
        }

        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

        if(isGrounded && (x!=0 || z!=0))
        {
            timeSinceLastStep += Time.deltaTime;
            if(timeSinceLastStep==0 || timeSinceLastStep >= walkSpeed/(speed*stepsPerSec))
            {
                if(speed==sprintSpeed) MoveSound("run");
                else MoveSound("walk");
                timeSinceLastStep = 0;
            }
        }
        else if(timeSinceLastStep!=0) timeSinceLastStep = 0;

        if(isGrounded && !wasGrounded)
        {
            if(speed==sprintSpeed) MoveSound("run");
            else MoveSound("walk");
        }
        wasGrounded = isGrounded;
    }

    void MoveSound(string type)
    {
        if(soundReady)
        {
            if(type=="walk") walks[Random.Range(0,walks.Length)].Play();
            else if(type=="run") runs[Random.Range(0,runs.Length)].Play();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.name == "MeleeRange")
            other.transform.parent.GetComponent<Animator>().SetBool("isPlayerClose", true);
    }

    void OnTriggerExit(Collider other)
    {
        if(other.name == "MeleeRange")
            other.transform.parent.GetComponent<Animator>().SetBool("isPlayerClose", false);
    }

    public void Hit()
    {
        conductor.Strike();
    }
}