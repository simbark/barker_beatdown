﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour
{
	public float fadeTime;
    public AudioSource[] kills;

    Music musicScript;
	float deadTime;
	float opacity;

    void Start()
    {
        deadTime = 0f;
        opacity = 255f;
        musicScript = GameObject.Find("/System").GetComponent<Music>();
        if(musicScript.isFighting()) kills[Random.Range(0,kills.Length)].Play();
    }

    void Update()
    {
        deadTime += Time.deltaTime;
        if(deadTime > 2.5) opacity -= 255 * Time.deltaTime / fadeTime;
        foreach(Renderer renderer in GetComponentsInChildren<Renderer>())
        	renderer.material.color = new Color32(255, 255, 255, (byte) opacity);
        if(opacity<=0) Destroy(this.gameObject);
    }
}
