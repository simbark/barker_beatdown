﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class Gun : MonoBehaviour
{
	public Camera viewCam;
	public Image crosshair;
	public LayerMask layerMask;
	public Text bulletText;
	public Text clipText;
	public int intBullets;
	public Renderer spark;
	public Renderer spark2;
	public float fadeTime;
	public AudioSource fireSound;
	public AudioSource reloadSound;
    public Music musicSystem;
    public GameObject blood;
    public GameObject debris;
    public Conductor conductor;
	
	Color32 white = new Color32(255, 255, 255, 200);
	Color32 red = new Color32(255, 0, 0, 200);
	Vector3 intPos;
	int bullets;
	float opacity;

	void Start()
	{
		bullets = intBullets;
		bulletText.text = "" + bullets;
		clipText.text = "/" + bullets;
		intPos = transform.localPosition;
		opacity = 0;
		spark.material.color = new Color32(255, 255, 255, (byte) opacity);
		spark2.material.color = new Color32(255, 255, 255, (byte) opacity);
	}

    void Update()
    {
    	crosshair.color = white;
    	RaycastHit hit;
    	if(Physics.Raycast(viewCam.transform.position, viewCam.transform.forward, out hit, Mathf.Infinity, layerMask))
    		if(hit.transform.name == "EnemyHitbox") crosshair.color = red;

        if(Input.GetButtonDown("Fire1"))
        {
        	if(bullets > 0) Shoot();
        	else Reload();
        }

        if(transform.localPosition.z < .66) transform.localPosition = new Vector3(intPos.x, intPos.y, transform.localPosition.z + Time.deltaTime);
        else transform.localPosition = intPos;

        if(opacity>0) opacity -= 255 * Time.deltaTime / fadeTime;
        else opacity = 0;
        spark.material.color = new Color32(255, 255, 255, (byte) opacity);
        spark2.material.color = new Color32(255, 255, 255, (byte) opacity);
        
    }

    void Shoot()
    {
    	fireSound.pitch = Random.Range(0.8f, 1.2f);
    	fireSound.Play();

    	transform.localPosition = new Vector3(intPos.x, intPos.y, .5f);
    	opacity = 255;
    	spark.material.color = new Color32(255, 255, 255, (byte) opacity);
    	spark2.material.color = new Color32(255, 255, 255, (byte) opacity);

    	bullets -= 1;
    	bulletText.text = "" + bullets;

    	RaycastHit hit;
    	if(Physics.Raycast(viewCam.transform.position, viewCam.transform.forward, out hit, Mathf.Infinity, layerMask))
    	{
    		if(hit.transform.name == "EnemyHitbox")
    		{
    			killThem(hit.transform.parent);
    			Destroy(hit.transform.gameObject);
                MakeParticle("blood", hit.point);
                if(musicSystem.Valid()) conductor.GetPoints(hit.distance);
    		}
    		else 
            {
                conductor.ResetMult();
                MakeParticle("debris", hit.point);
            }
    	}
        else conductor.ResetMult();

        if(!musicSystem.Valid()) conductor.Strike();
    }

    void killThem(Transform hit)
    {
    	hit.GetComponent<Animator>().enabled = false;
    	hit.GetComponent<Enemy>().enabled = false;
    	hit.GetComponent<NavMeshAgent>().enabled = false;
    	hit.GetComponent<Fade>().enabled = true;
    }

    void Reload()
    {
    	reloadSound.Play();
    	bullets = intBullets;
    	bulletText.text = "" + bullets;
    }

    void MakeParticle(string type, Vector3 pos)
    {
        GameObject toMake;
        if(type=="blood") toMake = blood;
        else toMake = debris;
        GameObject particle = Instantiate(toMake, pos, Quaternion.identity);
    }
}
