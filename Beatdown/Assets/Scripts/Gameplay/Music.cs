﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Music : MonoBehaviour
{
    public Conductor conductor;
    public AudioSource metronome;
    public AudioMixerSnapshot defaultMix;
    public AudioMixerSnapshot lowpass;
	public Transform tempo;
    public Transform progress;
    public Transform sectionBar;
    public Transform barA;
    public Transform barZ;
    public Transform enemies;
    public Text countdown;
    public Image fade;
    public GameObject line;
    public AudioSource[] songs;
    public EdgeDetect camScript;
    public GameObject[] inactiveAtStart;

    AudioSource bgm;
    float bpm;
    float offset = 0.045f;
	float spb;
    float songPos;
    float songLength;
    float timeSinceLastBeat;
    float dspSongTime;
    float lastStreak = -1f;
    float shotFired;
    float loopOffset;
    float fadeOpacity;
    float brightness = 1f;
    float[] section;
    int beatPos;
    int lastBeat = -1;
    int beat;
    int sectionNum = -1;
    int lastScore;
    int strike;
    int lastKills;
    bool fighting;
    bool needToRepeat;
    bool isEnding;

    static float[] bpms = {106.12f, 100f, 119.98f, 147f, 106.12f};
    static float[] section1 = {11.307f,38.447f,1*60+14.632f,1*60+50.817f,2*60+27.003f,3*60+12.235f,3*60+39.374f,4*60+15.559f};
    static float[] section2 = {19.2f,57.6f,1*60+36f,2*60+14.4f,2*60+52.8f};
    static float[] section3 = {8.001f,40.006f,1*60+28.014f,2*60+16.022f,2*60+52.028f,3*60+24.034f,3*60+56.039f};
    static float[] section4 = {1.632f,40.816f,1*60+6.938f,1*60+33.061f,1*60+59.183f,2*60+25.306f,2*60+51.428f,3*60+24.081f};
    static float[] section5 = {11.307f,38.447f,1*60+14.632f,1*60+50.817f,2*60+27.003f,3*60+12.235f,3*60+39.374f,4*60+6.513f,
    	  					   4*60+25.159f,5*60+3.559f,5*60+41.959f,6*60+20.359f,
    				    	   6*60+39.559f,7*60+11.565f,7*60+59.573f,8*60+47.581f,9*60+23.587f,9*60+55.592f,
    				    	   10*60+25.597f,11*60+4.781f,11*60+30.903f,11*60+57.025f,12*60+23.148f,12*60+49.270f,13*60+15.393f,13*60+48.046f};
    static float[][] sections = {section1,section2,section3,section4,section5};
    static float[] bpmChange5 = {4*60+15.559f,6*60+39.559f,10*60+25.597f};

    void Start()
    {
    	bgm = songs[Variables.currentSong];
    	bpm = bpms[Variables.currentSong];
    	section = sections[Variables.currentSong];
    	fade.enabled = false;

        defaultMix.TransitionTo(0f);
        spb = 60f/bpm;
        songLength = bgm.clip.length;
        dspSongTime = (float) AudioSettings.dspTime;
        shotFired = section[0];

        foreach(float marker in section)
        {
        	GameObject newLine = Instantiate(line, progress.parent);
        	float theY = marker/songLength * 250;
        	newLine.transform.localPosition = new Vector3(0, theY-125, 0);
        }
        barA.transform.localScale = new Vector3(1, section[0]/songLength, 1);
        barZ.transform.localScale = new Vector3(1, 1-section[section.Length-1]/songLength, 1);

        bgm.Play();
    }

    void Update()
    {
        songPos = (float)(AudioSettings.dspTime - dspSongTime - offset - loopOffset);
        if(songPos>songLength) songPos = songLength;
        progress.localScale = new Vector3(1, songPos/songLength, 1);

        for (int i=section.Length-1; i>=0; i--) if(songPos>=section[i] && sectionNum<i)
	    {
	      	if(needToRepeat)
	       	{
	       		needToRepeat = false;
	       		progress.GetComponent<Image>().color = new Color32(255,255,255,255);
	       		conductor.SetScore(lastScore);
	       		conductor.SetKills(lastKills);
	       		bgm.time = section[sectionNum];
	       		loopOffset += songPos - bgm.time;
	       	}
	       	else
	       	{
	       		lastScore = conductor.GetScore();
	       		lastKills = conductor.GetKills();
	       		sectionNum = i;
	       		sectionBar.localScale = progress.localScale;
	       		conductor.ResetStrikes();
	       	}
	       	break;
	    }

	    if(Variables.currentSong==4)
	    {
	    	if(songPos>=bpmChange5[2]) bpm = 147f;
	    	else if(songPos>=bpmChange5[1]) bpm = 119.98f;
	    	else if(songPos>=bpmChange5[0]) bpm = 100f;
	    	spb = 60f/bpm;
	    }

        if(Input.GetKeyDown("m")) metronome.mute = !metronome.mute;
        beatPos = Mathf.FloorToInt(songPos/spb);
        if(beatPos>lastBeat)
        {
            beat += 1;
            if(beat==5) beat = 1;
            if(tempo.gameObject.activeSelf) Beat(beat);
        }
        lastBeat = beatPos;

        if(lastStreak!=-1 && songPos > lastStreak + spb*2.5)
        {
            lastStreak = -1;
            conductor.ResetMult();
        }
        if(shotFired<=songPos-spb*16)
        {
        	shotFired = songPos;
        	conductor.Strike();
        }
        if(songPos>=section[section.Length-1] && shotFired!=songLength)
        	shotFired = songLength;

        if(songPos>=section[0]) countdown.text = "";
        else if(songPos>=section[0]-spb) countdown.text = "1";
        else if(songPos>=section[0]-spb*2) countdown.text = "2";
        else if(songPos>=section[0]-spb*3) countdown.text = "3";
        else if(songPos>=section[0]-spb*4) countdown.text = "4";
		if(songPos>=section[0]-4f && songPos<section[section.Length-1] && !fighting)
        	fighting = true;     
        if(songPos>=section[0]-spb/2 && songPos<section[section.Length-1] && !inactiveAtStart[0].activeSelf)
			foreach(GameObject obj in inactiveAtStart) obj.SetActive(true);
		if(songPos>=section[section.Length-1] && inactiveAtStart[0].activeSelf && !needToRepeat)
		{
			tempo.gameObject.SetActive(false);
			killAllEnemies();
		}
        if(!bgm.isPlaying && songPos>=section[section.Length-1] && !isEnding)
        {
            isEnding = true;
            fade.enabled = true;
            fadeOpacity = 0f;
        }
        if(isEnding)
        {
            fadeOpacity += 255 * Time.deltaTime / 2f;
            fade.color = new Color32(0,0,0,(byte)fadeOpacity);
            if(fadeOpacity>=255f)
		    {
		        Variables.lastScore = conductor.GetScore();
		        Variables.lastStrikes = strike;
		        Variables.kills = conductor.GetKills();
		        SceneManager.LoadScene("EndScreen", LoadSceneMode.Single);
		    }
        }

        if(songPos>=section[section.Length-1]) brightness=1;
        else
        {
        	brightness -= Time.deltaTime /2 /spb;
        	if(brightness<=0) brightness = 0f;
        }
        camScript.outlineColor = Color.HSVToRGB(0,0,brightness);
    }

    void Beat(int b)
    {
        metronome.Play();
    	tempo.Find("Beat" + b).GetComponent<Tempo>().Pulse();
    	switch(b)
    	{
    		case 2:
    			brightness = .95f;
    			break;
    		case 3:
    			brightness = .9f;
    			break;
    		case 4:
    			brightness = .85f;
    			break;
    		default:
    			brightness = 1f;
    			break;
    	}
    }

    public bool Valid()
    {
        float beatPosFloat = songPos/spb;
        return Mathf.Abs((beatPosFloat - Mathf.FloorToInt(beatPosFloat)) - .5f) > .25f;
    }

    public void Streak(bool yes)
    {
        if(yes) lastStreak = songPos;
        else lastStreak = -1;
        shotFired = songPos;
    }

    public bool Streaking()
    {
        return lastStreak!=-1;
    }

    public void Strike()
    {
        strike += 1;
    	lowpass.TransitionTo(spb);
    	needToRepeat = true;
    	progress.GetComponent<Image>().color = new Color32(255,0,0,255);
    }

    public void Unstrike()
    {
    	defaultMix.TransitionTo(spb);
    }

    public bool isFighting()
    {
    	return fighting;
    }

    void killAllEnemies()
    {
    	fighting = false;
		foreach(GameObject obj in inactiveAtStart) obj.SetActive(false);
        for(int i=0; i<enemies.childCount; i++)
        {
        	Transform enemy = enemies.GetChild(i);
    		Destroy(enemy.GetChild(enemy.childCount-1).gameObject);
        	enemy.GetComponent<Animator>().enabled = false;
    		enemy.GetComponent<Enemy>().enabled = false;
    		enemy.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;
    		enemy.GetComponent<Fade>().enabled = true;
    	}
    }
}
