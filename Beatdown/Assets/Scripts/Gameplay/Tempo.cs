﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tempo : MonoBehaviour
{
	float fadeTime;
	Image fill;
	float size;
	float opacity;

	void Start()
	{
		fadeTime = .25f;
		fill = transform.Find("White").GetComponent<Image>();
		size = 2f;
		opacity = 0;
	}

	void Update()
	{
		if(size>2f)
		{
			size -= Time.deltaTime / fadeTime;
			opacity -= 255 * Time.deltaTime / fadeTime;
		}
		else
		{
			size = 2f;
			opacity = 0;
		}
		DrawBox();
	}

    public void Pulse()
    {
    	opacity = 255;
    	size = 3f;
    	DrawBox();
    }

    void DrawBox()
    {
    	fill.color = new Color32(255, 255, 255, (byte) opacity);
    	transform.localScale = new Vector3(size, size, 1);
    }
}
