﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartTrigger : MonoBehaviour
{
	public Conductor conductor;
	public GameObject frontDoor;

    void OnTriggerEnter(Collider other)
    {
    	if(other.gameObject.name=="Player")
    	{
    		conductor.StartGame();
    		frontDoor.SetActive(true);
    		Destroy(gameObject);
    	}
    }
}
