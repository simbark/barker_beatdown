using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
	public Animator animator;
    public NavMeshAgent nav;
    public float walkSpeed;
    public float runSpeed;
    public AudioSource hitSound;

	Transform player;

	void Start()
	{
		player = GameObject.Find("/Player").transform;
	}

    void Update()
    {
        animator.SetFloat("alive", animator.GetFloat("alive") + Time.deltaTime);
        animator.SetFloat("playerDist", Vector3.Distance(player.position, transform.position));

        nav.SetDestination(player.position);
        if(animator.GetBool("isPlayerClose")) Stop();
    }

    void Hit()
    {
    	if(animator.GetBool("isPlayerClose"))
    	{
    		hitSound.Play();
    		player.GetComponent<Player>().Hit();
    	}
    }
    void Stop()
    {
        nav.speed = 0;
    }
    void Walk()
    {
        nav.speed = walkSpeed;
    }
    void Run()
    {
        nav.speed = runSpeed;
    }
}
