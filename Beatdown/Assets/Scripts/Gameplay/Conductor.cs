using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Conductor : MonoBehaviour
{
	public Music musicScript;
	public Transform enemies;
	public Transform spawns;
	public Transform strikeMarkers;
	public GameObject enemy;
	public AudioSource strikeSound;
	public Text score;
	public Text multText;
	public Image fade;
	public int enemyCeiling;
	public float strikeBuffer;
    public Player control;
    public Aim aim;
    public GameObject[] inactiveAtStart;

	int multiplier = 1;
	int points = 0;
	int strikes = 0;
	int kills = 0;
	float fadeOpacity = 255f;
	float timeSinceLastStrike = 0f;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        foreach(GameObject obj in inactiveAtStart) obj.SetActive(false);
    }
    public void StartGame()
    {
        foreach(GameObject obj in inactiveAtStart) obj.SetActive(true);
    	musicScript.enabled = true;
    }

    void Update()
    {
    	if(fade.enabled)
    	{
    		fadeOpacity -= 255 * Time.deltaTime / 2f;
    		fade.color = new Color32(0,0,0,(byte)fadeOpacity);
    		if(fadeOpacity<=0)
            {
                control.enabled = true;
                aim.enabled = true;
                fade.enabled = false;
            }
    	}
        if(enemies.childCount<Variables.enemyCount[Variables.currentDifficulty] && musicScript.isFighting())
        {
        	Transform where = spawns.GetChild(Random.Range(0,spawns.childCount));
        	GameObject zombie = Instantiate(enemy, where.position, where.rotation);
        	zombie.transform.parent = enemies;
        }
        timeSinceLastStrike += Time.deltaTime;
    }

    public void GetPoints(float dist)
    {
        musicScript.Unstrike();
    	if(musicScript.Streaking())
    	{
    		multiplier += 1;
    		if(multiplier > 5) multiplier = 5;
    		multText.text = "x" + multiplier;
    	}
    	else ResetMult();
    	musicScript.Streak(true);
    	
    	if(dist>=10f) points += 100 * multiplier;
    	else if(dist>=2.5f) points += 150 * multiplier;
    	else points += 200 * multiplier;
    	score.text = points.ToString();

    	kills += 1;
    }

    public void ResetMult()
    {
    	multiplier = 1;
    	multText.text = "";
    	musicScript.Streak(false);
    }

    public void Strike()
    {
    	ResetMult();
    	if(timeSinceLastStrike>strikeBuffer)
    	{
    		strikes += 1;
            if(strikes==3) SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
            else
            {
                timeSinceLastStrike = 0;
                strikeSound.Play();
                for(int s=0; s<strikes; s++) strikeMarkers.GetChild(s).GetComponent<Image>().enabled = true;
                musicScript.Strike();
            }
    	}
    }
    public void ResetStrikes()
    {
        strikes = 0;
        for(int s=0; s<3; s++) strikeMarkers.GetChild(s).GetComponent<Image>().enabled = false;
    }

    public int GetScore()
    {
        return points;
    }

    public void SetScore(int p)
    {
        points = p;
        score.text = points.ToString();
    }
    public int GetKills()
    {
    	return kills;
    }
    public void SetKills(int k)
    {
        kills = k;
    }
}
