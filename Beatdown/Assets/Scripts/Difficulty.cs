using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Difficulty : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public string id;
	public AudioSource mouseover;
	public AudioSource click;

	Image highlight;
	static Image fade;
	static bool going;
	static float opacity;

	void Start()
	{
		going = false;
		opacity = 0f;
		highlight = gameObject.transform.GetChild(2).GetComponent<Image>();
		fade = GameObject.Find("/Canvas/Black").GetComponent<Image>();
	}

	void Update()
	{
		if(Input.GetMouseButtonDown(0) && !going && highlight.color == new Color32(255,255,255,127))
		{
			going = true;
			click.Play();
			highlight.color = new Color32(0,0,0,127);

			if(id[0]=='n') Variables.currentDifficulty = 1;
			else if(id[0]=='i') Variables.currentDifficulty = 2;
			else Variables.currentDifficulty = 0;

			if(id[1]=='2') Variables.currentSong = 1;
			else if(id[1]=='3') Variables.currentSong = 2;
			else if(id[1]=='4') Variables.currentSong = 3;
			else if(id[1]=='5') Variables.currentSong = 4;
			else Variables.currentSong = 0;
		}
		if(going)
		{
			opacity += 255 * Time.deltaTime / 4;
			if(opacity>255) opacity = 255;
			fade.color = new Color32(0,0,0,(byte)opacity);
			if(opacity==255) SceneManager.LoadScene("Play", LoadSceneMode.Single);
		}
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
    	if(!going)
    	{
    		highlight.color = new Color32(255,255,255,127);
    		mouseover.Play();
    	}
	}

	public void OnPointerExit(PointerEventData eventData)
	{
    	if(!going) highlight.color = new Color32(255,255,255,0);
	}
}
