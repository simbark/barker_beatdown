﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Flash : MonoBehaviour
{
	float timer;
	Color thisColor;

    void Start()
    {
    	thisColor = GetComponent<Text>().color;
    }

    void Update()
    {
    	timer += Time.deltaTime;
    	if(timer > .5f) timer = 0;
    	if(timer <= .25f) GetComponent<Text>().color = new Color32(0,0,0,0);
    	else GetComponent<Text>().color = thisColor;
    }
}
