﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Preview : MonoBehaviour, IPointerEnterHandler
{
	public int id;
	public Transform ind;
	public AudioSource mouseover;
	public AudioSource[] previews;

	void Update()
	{
		bool mute = true;
		foreach(AudioSource pv in previews) if(pv.isPlaying) mute = false;
		if(mute) ind.position = new Vector3(-1000, ind.position.y, ind.position.z);
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
    	mouseOver();
    }

    void mouseOver()
    {
    	mouseover.Play();
    	ind.position = new Vector3(transform.position.x, ind.position.y, ind.position.z);
    	if(!previews[id].isPlaying)
    	{
    		foreach(AudioSource pv in previews) pv.Stop();
    		previews[id].Play();
    	}
    }
}
