using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutorialButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public string type;
	public AudioSource mouseover;
	public GameObject[] pages;
	public GameObject Return1;
	public GameObject Return2;
	public GameObject Prev;
	public GameObject Next;

	GameObject fill;
	bool moused;
	static int page;

    void Start()
    {
        fill = gameObject.transform.GetChild(1).gameObject;
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0) && moused)
        {
        	moused = false;
        	fill.SetActive(false);
        	pages[page].SetActive(false);
        	if(type=="next") page+=1;
        	else page-=1;
        	pages[page].SetActive(true);

        	Prev.SetActive(page!=0);
        	Next.SetActive(page!=5);
        	Return1.SetActive(!Prev.activeSelf);
        	Return2.SetActive(!Next.activeSelf);
        }
    }

	public void OnPointerEnter(PointerEventData eventData)
	{
		mouseover.Play();
		moused = true;		
		fill.SetActive(true);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
    	moused = false;
    	fill.SetActive(false);
	}
}
