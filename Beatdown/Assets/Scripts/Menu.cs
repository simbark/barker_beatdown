using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class Menu : MonoBehaviour
{
	public Text[] Xrank1;
	public Text[] Xrank2;
	public Text[] Xrank3;
	public Text[] Xrank4;
	public Text[] Xrank5;
	public Text[] Xscore;
	public GameObject[] Xunlocks;
	public GameObject[] Xlocks;
    public AudioMixerSnapshot defaultMix;

    int delCount;

    void Start()
    {
        loadData();

        defaultMix.TransitionTo(0f);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    	for(int i=0; i<3; i++)
    	{
    		Xrank1[i].text = "" + Variables.rank1[i];
    		Xrank2[i].text = "" + Variables.rank2[i];
    		Xrank3[i].text = "" + Variables.rank3[i];
    	}
    	Xrank4[0].text = "" + Variables.rank4[0];
    	Xrank4[1].text = "" + Variables.rank4[1];
    	Xrank5[0].text = "" + Variables.rank5[0];
        for(int i=0; i<5; i++) Xscore[i].text = "" + Variables.score[i];

        if((Variables.rank1[1]!='-' || Variables.rank1[2]!='-') && (Variables.rank2[1]!='-' || Variables.rank2[2]!='-') && (Variables.rank3[1]!='-' || Variables.rank3[2]!='-'))
        {
        	Xunlocks[0].SetActive(true);
        	Xlocks[0].SetActive(false);
        }
        if(Variables.rank1[2]!='-' && Variables.rank2[2]!='-' && Variables.rank3[2]!='-' && Variables.rank4[1]!='-')
        {
        	Xunlocks[1].SetActive(true);
        	Xlocks[1].SetActive(false);
        }

        if(Xunlocks[0].activeSelf)
        	Xlocks[1].transform.GetChild(2).GetComponent<Text>().text = "Finish all previous tracks on Insane difficulty to unlock the final track.";
    }

    void loadData()
    {
        char[] rankKey = {'M','X','S','A','B','C','D','E','F','-'};
        PlayerPrefs.SetInt("rankW1",System.Array.IndexOf(rankKey,Variables.rank1[0]));
        PlayerPrefs.SetInt("rankN1",System.Array.IndexOf(rankKey,Variables.rank1[1]));
        PlayerPrefs.SetInt("rankI1",System.Array.IndexOf(rankKey,Variables.rank1[2]));
        PlayerPrefs.SetInt("rankW2",System.Array.IndexOf(rankKey,Variables.rank2[0]));
        PlayerPrefs.SetInt("rankN2",System.Array.IndexOf(rankKey,Variables.rank2[1]));
        PlayerPrefs.SetInt("rankI2",System.Array.IndexOf(rankKey,Variables.rank2[2]));
        PlayerPrefs.SetInt("rankW3",System.Array.IndexOf(rankKey,Variables.rank3[0]));
        PlayerPrefs.SetInt("rankN3",System.Array.IndexOf(rankKey,Variables.rank3[1]));
        PlayerPrefs.SetInt("rankI3",System.Array.IndexOf(rankKey,Variables.rank3[2]));
        PlayerPrefs.SetInt("rankN4",System.Array.IndexOf(rankKey,Variables.rank4[0]));
        PlayerPrefs.SetInt("rankI4",System.Array.IndexOf(rankKey,Variables.rank4[1]));
        PlayerPrefs.SetInt("rankI5",System.Array.IndexOf(rankKey,Variables.rank5[0]));
        Variables.rank1[0] = rankKey[PlayerPrefs.GetInt("rankW1")];
        Variables.rank1[1] = rankKey[PlayerPrefs.GetInt("rankN1")];
        Variables.rank1[2] = rankKey[PlayerPrefs.GetInt("rankI1")];
        Variables.rank2[0] = rankKey[PlayerPrefs.GetInt("rankW2")];
        Variables.rank2[1] = rankKey[PlayerPrefs.GetInt("rankN2")];
        Variables.rank2[2] = rankKey[PlayerPrefs.GetInt("rankI2")];
        Variables.rank3[0] = rankKey[PlayerPrefs.GetInt("rankW3")];
        Variables.rank3[1] = rankKey[PlayerPrefs.GetInt("rankN3")];
        Variables.rank3[2] = rankKey[PlayerPrefs.GetInt("rankI3")];
        Variables.rank4[0] = rankKey[PlayerPrefs.GetInt("rankN4")];
        Variables.rank4[1] = rankKey[PlayerPrefs.GetInt("rankI4")];
        Variables.rank5[0] = rankKey[PlayerPrefs.GetInt("rankI5")];
        PlayerPrefs.SetInt("score1",Variables.score[0]);
        PlayerPrefs.SetInt("score2",Variables.score[1]);
        PlayerPrefs.SetInt("score3",Variables.score[2]);
        PlayerPrefs.SetInt("score4",Variables.score[3]);
        PlayerPrefs.SetInt("score5",Variables.score[4]);
        Variables.score[0] = PlayerPrefs.GetInt("score1");
        Variables.score[0] = PlayerPrefs.GetInt("score2");
        Variables.score[0] = PlayerPrefs.GetInt("score3");
        Variables.score[0] = PlayerPrefs.GetInt("score4");
        Variables.score[0] = PlayerPrefs.GetInt("score5");
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Delete)) delCount += 1;
        if(delCount==5)
        {
        	Variables.rank1[0] = Variables.rank1[1] = Variables.rank1[2] = '-';
			Variables.rank2[0] = Variables.rank2[1] = Variables.rank2[2] = '-';
			Variables.rank3[0] = Variables.rank3[1] = Variables.rank3[2] = '-';
			Variables.rank4[0] = Variables.rank4[1] = Variables.rank5[0] = '-';
			Variables.score[0] = Variables.score[1] = Variables.score[2] = Variables.score[3] = Variables.score[4] = 0;

            loadData();
            PlayerPrefs.Save();
            Application.LoadLevel(0);
        }
    }
}
