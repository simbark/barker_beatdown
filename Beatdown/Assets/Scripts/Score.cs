﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class Score : MonoBehaviour
{
	public AudioMixerSnapshot lowpass;
    public AudioSource[] previews;
	public Text rank;
	public Text score;

    void Start()
    {
    	if(Variables.currentSong<4)
    	{
    		lowpass.TransitionTo(0f);
    		previews[Variables.currentSong].Play();
    	}

    	// rank
    	char[] ranks = {'M','X','S','A','B','C','D','E','F','-'};
    	int pos = 4;
    	if(Variables.kills >= Variables.beats[Variables.currentSong]) pos = 0;
    	else if(Variables.kills >= Variables.beats[Variables.currentSong]*3/4) pos = 1;
    	else if(Variables.kills >= Variables.beats[Variables.currentSong]/2) pos = 2;
    	else if(Variables.kills >= Variables.beats[Variables.currentSong]/4) pos = 3;
    	pos += Variables.lastStrikes;
    	if(pos>8) pos=8;
    	rank.text = "" + ranks[pos];
    	switch(Variables.currentSong)
    	{
    		case 1:
    			Variables.rank2[Variables.currentDifficulty] = ranks[pos];
    			break;
    		case 2:
    			Variables.rank3[Variables.currentDifficulty] = ranks[pos];
    			break;
    		case 3:
    			Variables.rank4[Variables.currentDifficulty-1] = ranks[pos];
    			break;
    		case 4:
    			Variables.rank5[0] = ranks[pos];
    			break;
    		default:
    			Variables.rank1[Variables.currentDifficulty] = ranks[pos];
    			break;
    	}

    	// score
        score.text = "" + Variables.lastScore;
        Variables.score[Variables.currentSong] = Variables.lastScore;

        // reset temp vars
        Variables.currentSong = 0;
        Variables.currentDifficulty = 0;
        Variables.lastScore = 0;
        Variables.lastStrikes = 0;
        Variables.kills = 0;

        // save
        PlayerPrefs.SetInt("rankW1", System.Array.IndexOf(ranks,Variables.rank1[0]));
        PlayerPrefs.SetInt("rankN1", System.Array.IndexOf(ranks,Variables.rank1[1]));
        PlayerPrefs.SetInt("rankI1", System.Array.IndexOf(ranks,Variables.rank1[2]));
        PlayerPrefs.SetInt("rankW2", System.Array.IndexOf(ranks,Variables.rank2[0]));
        PlayerPrefs.SetInt("rankN2", System.Array.IndexOf(ranks,Variables.rank2[1]));
        PlayerPrefs.SetInt("rankI2", System.Array.IndexOf(ranks,Variables.rank2[2]));
        PlayerPrefs.SetInt("rankW3", System.Array.IndexOf(ranks,Variables.rank3[0]));
        PlayerPrefs.SetInt("rankN3", System.Array.IndexOf(ranks,Variables.rank3[1]));
        PlayerPrefs.SetInt("rankI3", System.Array.IndexOf(ranks,Variables.rank3[2]));
        PlayerPrefs.SetInt("rankN4", System.Array.IndexOf(ranks,Variables.rank4[0]));
        PlayerPrefs.SetInt("rankI4", System.Array.IndexOf(ranks,Variables.rank4[1]));
        PlayerPrefs.SetInt("rankI5", System.Array.IndexOf(ranks,Variables.rank5[0]));
        PlayerPrefs.SetInt("score1", Variables.score[0]);
        PlayerPrefs.SetInt("score2", Variables.score[1]);
        PlayerPrefs.SetInt("score3", Variables.score[2]);
        PlayerPrefs.SetInt("score4", Variables.score[3]);
        PlayerPrefs.SetInt("score5", Variables.score[4]);
        PlayerPrefs.Save();
    }
}
