﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public string type;
	public AudioSource mouseover;

	GameObject fill;
	bool moused;

    void Start()
    {
        fill = gameObject.transform.GetChild(1).gameObject;
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0) && moused)
        {
        	moused = false;

        	switch(type)
        	{
        		case "tutorial":
        			SceneManager.LoadScene("Tutorial", LoadSceneMode.Single);
        			break;
        		case "quit":
        			Application.Quit();
        			break;
        		default:
        			SceneManager.LoadScene("Menu", LoadSceneMode.Single);
        			break;
        	}
        }
    }

	public void OnPointerEnter(PointerEventData eventData)
	{
		moused = true;
		mouseover.Play();
		fill.SetActive(true);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
    	moused = false;
    	fill.SetActive(false);
	}
}