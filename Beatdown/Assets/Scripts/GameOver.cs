﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    void Update()
    {
        if(Input.GetKey(KeyCode.Space)) SceneManager.LoadScene("Play", LoadSceneMode.Single);
        if(Input.GetKey(KeyCode.Backspace)) SceneManager.LoadScene("Menu", LoadSceneMode.Single);
    }
}
