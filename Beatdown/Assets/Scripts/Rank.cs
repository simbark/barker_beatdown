using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rank : MonoBehaviour
{
	public Material gold;
	public Material silver;
	public Material bronze;

    void Update()
    {
        switch(GetComponent<Text>().text)
        {
        	case "M":
        		GetComponent<Text>().material = gold;
        		GetComponent<Text>().color = new Color32(255,255,255,255);
        		break;
        	case "X":
        		GetComponent<Text>().material = silver;
        		GetComponent<Text>().color = new Color32(255,255,255,255);
        		break;
        	case "S":
        		GetComponent<Text>().material = bronze;
        		GetComponent<Text>().color = new Color32(255,255,255,255);
        		break;
        	case "A":
        		GetComponent<Text>().material = null;
        		GetComponent<Text>().color = new Color32(255,31,31,255);
        		break;
        	case "B":
        		GetComponent<Text>().material = null;
        		GetComponent<Text>().color = new Color32(230,230,51,255);
        		break;
        	case "C":
        		GetComponent<Text>().material = null;
        		GetComponent<Text>().color = new Color32(65,204,65,255);
        		break;
        	case "D":
        		GetComponent<Text>().material = null;
        		GetComponent<Text>().color = new Color32(75,178,178,255);
        		break;
        	case "E":
        		GetComponent<Text>().material = null;
        		GetComponent<Text>().color = new Color32(80,80,153,255);
        		break;
        	case "F":
        		GetComponent<Text>().material = null;
        		GetComponent<Text>().color = new Color32(128,81,128,255);
        		break;
        	default:
        		GetComponent<Text>().material = null;
        		GetComponent<Text>().color = new Color32(255,255,255,255);
        		break;
        }
    }
}
