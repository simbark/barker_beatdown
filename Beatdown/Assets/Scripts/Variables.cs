using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Variables
{
	public static int currentSong = 0;
	public static int currentDifficulty = 0;
	public static int lastScore = 0;
	public static int lastStrikes = 0;
	public static int kills = 0;

	public static int[] enemyCount = {10,20,30};
	public static int[] beats = {432,256,456,496,1620};

	public static char[] rank1 = {'-','-','-'};
	public static char[] rank2 = {'-','-','-'};
	public static char[] rank3 = {'-','-','-'};
	public static char[] rank4 = {'-','-'};
	public static char[] rank5 = {'-'};
	public static int[] score = {0,0,0,0,0};
}
